---
title: '5G, la somme de toutes les peurs'
author: Damien Liccia
date: '2020-04-11 11:15:00+02:00'
lastmod: '2020-04-11 11:20:00+02:00'
slug: 5g-la-somme-de-toutes-les-peurs
categories:
  - Science
  - Théories du complot
tags:
  - peur
  - rumeur
  - technologie
  - 5G
draft: no
type: post 
image: 5G_logo.jpg
description: "Analyse des théories alternatives suscitées par la 5G, et qui appréhendent notamment cette dernière comme étant à l'origine de la pandémie de COVID-19"

---

_Cette brève est issue du [bulletin](https://www.antidox.fr/2020/04/10/veille-strategique-et-dopinion-digitale-covid-19-10-avril/) réalisé quotidiennement par [Antidox](https://www.antidox.fr/) et [ESL](https://www.eslnetwork.com/)._


À côté de la 5G et de l’imaginaire qu’elle véhicule et suscite, la controverse suscitée par les compteurs intelligents Linky et Gazpar, installés depuis maintenant plusieurs années par ENEDIS et GRDF, ferait presque figure de _canada dry_ ou, pour reprendre une dualité mémétique née sur les réseaux sociaux en ces journées de confinement, décidément propices à tous les inventions culturelles, de ["F1 versus Karting"](https://twitter.com/search?q=f1%20karting&src=typeahead_click).

Impact supposé nocif des ondes électromagnétiques, crainte d’un contrôle accru de la part des opérateurs sur le comportement des clients voire, et nous basculons dès lors dans un univers où se cristallisent toutes les peurs et les tous les fantasmes, craintes d’une manipulation des corps et des esprits : force est de constater que la controverse autour des compteurs intelligents est riche en enseignement quand il s’agit d’aborder celle relative à la 5G. 

Invisibles comme l’est le COVID-19, les ondes constituent pour certains un ennemi d’autant plus sournois et pernicieux qu’il est invisible et diffus. Dès lors, il n’en est que plus propice à être recouvert, par couches narratives successives (de la santé jusqu’aux considérations géopolitiques les plus confusionnistes), de tous les oripeaux de théories supposément alternatives, où alternent pensée critique et fantasmagories irrationnelles. 

>“La 5G affaiblit le système immunitaire”. “La 5G diffuse le COVID-19”. “La 5G est un vecteur
technologique à la guerre bactériologique que nous mènerait la Chine”. 

Après tout, entre méconnaissance de l’ennemi, à condition évidemment d’appréhender comme tel lesdites ondes, besoin de trouver un bouc émissaire, une victime expiatoire et tout autre ersatz de sens dans un contexte où toute cohérence semble avoir disparue (et pas uniquement au
sommet de l’État, comme d’aucuns sont enclins à le dire), la 5G pourrait jouer le rôle de causalité magique. Ces assertions, qui ne vont pas sans avoir des conséquences matérielles, dépassant ainsi les frontières numériques des réseaux sociaux, notamment au Royaume-Uni où des antennes relais ont récemment été incendiées, questionnent tout à la fois les réflexes psychologiques des sociétés humaines, notre rapport à l’information, ainsi que la place des réseaux sociaux dans notre rapport au réel. 

S’il convient de s’écarter des approches moralistes, hautaines et supérieures qui, d’un revers de main balayent comme dérisoires ce
type de manifestations, c’est pour restituer dans un cheminement intellectuel les origines de ces phénomènes ontologiques. 

>Dans son maître ouvrage, La Peur en Occident, paru en 1978,Jean Delumeau écrivait que “les projections iconographiques, sorte d’exorcisme au fléau,constituent, avec la fuite et l’agressivité, des réactions habituelles devant une peur qui se transforme en angoisse”. 

Si les projections iconographiques en temps de peste ou de choléra, dans des sociétés où le sens religieux constituait encore une réalité de tous les jours, des calvaires aux tympans des églises, peuplés d’évocation du Jugement dernier et autres récits édifiants et structurants, constituaient une projection iconographique et religieuse, il convient de s’interroger sur les projections narratives contemporaines où s’entremêlent dans un magma informe téléologies technologiques et discours alternatifs.
 
Par ailleurs, sur ce sujet j'ai notamment été interviewé par Dominique Desaunay de RFI, le mercredi 8 avril.

{{< tweet 1247839887010811904 >}}
