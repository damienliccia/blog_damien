---
title: COVID-19, relations publiques, influence et astroturfing
author: Damien Liccia
date: '2020-03-27 18:18:52'
lastmod: '2020-03-27 20:57:13'
slug: covid-19-relations-publiques-influence-astroturfing
categories:
  - Stratégies d'influence
tags:
  - COVID-19
  - relations publiques
  - influence
  - astroturfing
draft: no
type: post
image: 1585333525.png
description: "Si nous sommes en guerre contre le COVID-19, et si celle-ci cristallise tous les efforts, avec un front et un arrière, des guerres transverses se nouent en ce moment."
---

_Un article co-écrit avec Xavier Desmaison et Jean-Baptiste Delhomme, avec la contribution d’Edouard Schuppert._

__Si nous sommes en guerre contre le COVID-19, comme le martelait Emmanuel Macron dans son adresse à la nation du 16 mars dernier, et si cette dernière cristallise tous les efforts, avec un front et un arrière, des guerres transverses se nouent en ce moment. Elles sont de plus ou moins fortes intensités, et il n’est guère certain, au demeurant, que le vocable de guerre soit le plus à même de caractériser ces phénomènes, tant ils sont disparates. Il s’agit, pour certains acteurs, de disséminer des contre-narratifs et de reprendre le contrôle de l’histoire, et de là à essayer de la réécrire, il n’y a souvent qu’un pas. À ce jeu, les grandes puissances ne sont pas en reste, de la Chine, aux États-Unis en passant par l’Iran, voire même la Russie.__

Alors que l’Europe est dans l’oeil du cyclone de la pandémie de COVID-19, les images qui nous proviennent de Chine depuis début mars contrastent avec les tragédies qui se nouent sur le Vieux continent, pris de court par l’épidémie et, comme sonné par les coups de boutoir d’un ennemi invisible qui s’acharne sur la province de Bergamo. 

![Graphique : Évolution du Covid-19 (Nombre de décès)](/images/content/plot_evol.png)

Les images des départs, en rang par deux, des médecins, infirmiers et autres soignants chinois des hôpitaux de fortune construits à Wuhan, les chiffres communiqués quotidiennement qui tendent à confirmer que l’épidémie a été enrayée dans la province du Hubei ou encore les avions chinois qui arrivent en Lombardie chargés de matériels à destination des italiens, constituent autant de marqueurs forts et symboliques. On en oublierait presque que, comme la grippe espagnole il y a un siècle, le COVID-19 qui nous assaille, nous confine et fait trembler notre économie, vient de Chine. Une provenance que Donald Trump, toujours aussi prompt à tweeter, de son iPhone notamment, n’a pas oublié.

## Pour Trump, le virus n’est pas un ennemi invisible. Il a un nom : le “Chinese Virus”

![Graphique : Discussion sur Twitter autour su prétendu Chinese Virus](/images/content/plot_chinese_virus2020-03-23.png)

Donald Trump n’a pas inventé le vocable de “Chinese Virus”. La discussion, comme le montre le graphique ci-dessus, préexistait au tweet du Président américain en date 16 mars. La seule différence réside dans la portée que Donald Trump a donné à cette expression, pour le moins univoque. Entre le 12 et le 16 mars, nous relevions en moyenne 416 tweets organiques par heure utilisant cette expression. Les trois jours ayant suivi la publication de Trump ont donné lieu, quant à eux, à 4 687 tweets organiques par heure. Une conversation décuplée qui rappelle, s’il fallait en douter, la capacité du locataire de la Maison Blanche, à mettre à l’agenda des thématiques, plus ou moins oiseuses, en quelques tweets. D’ailleurs, et bien que cela puisse au premier abord sembler anecdotique, l’évolution de la densité des majuscules dans les publications de Trump est révélatrice de phénomènes politiques, juridiques, économiques ou, dorénavant, épidémiologiques saillants, pour le Président américain. 

![Graphique : Évolution de la densité de majuscules dans les tweets de Donald Trump](/images/content/p_3.png)

Même si la séquence n’est pas réductible exclusivement au COVID-19, avec notamment des prises de positions du Président américain dans le cadre de la primaire démocrates, avant que la pandémie ne relègue cette dernière à l’arrière-plan, force est de constater que Trump est d’autant plus enclin à utiliser la touche MAJ de son clavier en mars qu’en janvier ou février ; périodes pourtant marquées par la procédure de destitution le visant. Avec 7 mentions de l’expression “Chinese Virus” entre le 16 et le 22 mars, on ne peut que constater que Trump a tout fait pour orienter la colère de ses partisans, la “team MAGA”, dont il est toujours difficile de départir de ce qui relève du militant zélé et de l’astroturfing à grande échelle, vers la Chine.

## Conflit des narratifs et batailles rangées dans les travées de Twitter

Confinement oblige, jamais les réseaux sociaux n’auront occupé une place aussi centrale dans le façonnement de notre appréhension d’une problématique globale. S’il ne s’agit pas de tomber dans une vision parcellaire, qui tendrait à omettre le rôle de la télévision (35 millions de Français ont écouté Emmanuel Macron le 16 mars à 20h), les réseaux sociaux, et tout particulièrement Twitter, contribuent à créer une méta-réalité de la crise sanitaire. Twitter, et non pas uniquement parce qu’il est le plus accessible, apparaît à nouveau comme la plateforme majeure d’agrégation et de co-construction de l’information. En période de crise et d’incertitude, pour quiconque cherche à obtenir les dernières nouvelles, parfois fausses au demeurant (intentionnellement ou non), ou à obtenir de l’information à haute valeur ajoutée Twitter est une évidence. “Distanciation sociale”, “#FlattenTheCurve”, à travers l’exemple, aujourd’hui célèbre, de la comparaison entre la gestion par les villes de Saint-Louis et de Philadelphie de la grippe espagnole de 1918, notre imaginaire, et potentiellement celui de certains décideurs, est en partie façonné par ce que nous lisons sur Twitter. Après tout, Olivier Véran n’a-t-il pas tracé sur BFM TV, à une heure de grande écoute, la courbe représentant le lissage d’un pic épidémique, quelques heures seulement après l’émergence de ce _topic_ sur Twitter ?  

Dans cette dissémination de l’information, outre la polarisation du débat autour de la chloroquine défendue par le Professeur Didier Raoult, outre les tweets du néo-média _Conflits_ qui de manière récurrente nous rappelle que l’“ennemi”, bien qu’invisible, est toujours présent, la bataille des narratifs fait rage autour de l’action des États, de l’origine du COVID-19 et des ressorts de sa circulation.

![Graphique : activité sur Twitter des comptes des amabassades chinoises en France, en Italie et aux USA](/images/content/plot_compt.png)

Si les experts en désinformation mettent souvent au jour des officines et autres réseaux de comptes, engagés dans  des opérations d’_astroturfing_, plus ou moins automatisées et sophistiquées (souvent moins que plus d’ailleurs), la manière dont la Chine, via son écosystème de compte Twitter d’ambassades, essaye de contrecarrer et de contrebalancer les discours dépréciatifs à son égard est en train de devenir un cas d’école. Que ce soit en termes d’activité (tweets et retweets d’un compte), ou en termes d’engagements suscités (somme des retweets et des likes sur une période) on ne peut qu’être frappé par les metrics relevés sur la période allant du 9 au 24 mars pour des comptes comme celui de l’ambassade de Chine en France.

{{< tweet 1240267133206122497 >}}

Sur la période étudiée, le top tweet, de l’ambassade est constitué par une photo montrant les matériels de protection envoyés par la Chine à la France. Les réactions sont pour l’essentiel positives, même si certains utilisateurs profitent de cette aide pour l’opposer à la posture du gouvernement français en termes de gestion de crise, perçue, appréhendée par ces derniers comme caractérisée par une dimension erratique, voire pour basculer, tout bonnement, dans des formes de racisme et de xénophobie. Les éléments de langage de cette publication ont été repris par _RT France_, _Chine Nouvelle_, le _Quotidien du Peuple_ ou encore _CCTV France_. Une posture de bienveillance et de solidarité, dont on ne saurait cacher certaines finalités sous-jacentes, mais à ce jeu certaines prises de position “à chaud” d’acteurs du monde de l’entreprise ne sont pas moins exemptes d’arrières-pensées, qui tranche avec la tonalité prise par ce compte au cours de ces derniers jours. 

{{< tweet 1242486367172603904 >}}

Défense de la médecine traditionnelle chinoise dans le cadre du traitement du COVID-19, mise en avant de la célérité de la Chine pour enrayer la diffusion de l’épidémie, alors qu’au même moment, la seule Lombardie compte plus de morts que le foyer originel du COVID-19, et, plus récemment, mise en ligne de plusieurs publications que, pudiquement, nous qualifierons comme s’inscrivant pleinement dans la rhétorique des discours dits alternatifs.

{{< tweet 1242007742241521664 >}}

Que des militants, des _proxies_, des médias ou encore des officines gravitant autour d’un pouvoir, en Russie, en Chine ou ailleurs dans le monde, répandent des informations de ce type, cela n’a rien de bien nouveau. Qu’un compte officiel et institutionnel fasse sienne une telle rhétorique, cela ne manque pas de surprendre. Comme si, [reprendre le contrôle de l’image et du discours](https://www.frstrategie.org/publications/notes/un-nouveau-defi-pour-chine-reprendre-controle-image-discours-2020 "reprendre le contrôle de l’image et du discours"), pour reprendre le titre de Valérie Niquet, spécialiste de la Chine, pour la FRS, en date du 23 mars, passait pour la Chine par une approche tous azimuts. Des comptes institutionnels, en passant par les “médias amis”, les “soutiens locaux” et les comptes Twitter plus ou moins authentiques.

## #MerciLaChine, une nouvelle manifestation du  “let nothing pass” chinois ?

![Graphique : Néo-comptes actifs dans la discussion autour de #MerciLaChine](/images/content/neo_users.png)

Par ailleurs, en analysant les comptes actifs dans la discussion autour du #MerciLaChine, un hashtag ayant circulé sur Twitter sur les journées des 18 et 19 mars, qui a suscité en premier lieu des réactions relativement positives à l’égard du pays, avant que des utilisateurs, dans une pure logique de _trolling_ ne dévoie ce dernier pour en faire un creuset de discours racistes et xénophobes, on constate que certains utilisateurs pro-chinois ont eu un comportement étonnant.

Nous avons ainsi relevé tout un écosystème de comptes, ou plus précisément de néo-comptes, très actifs sur ce hashtag. Le fait d’être un néo-compte actif sur ce _topic_ n’a rien de suspect ou de problématique en soi. 

Après tout, les séquences de forte incertitude et de tension sont propices à l’arrivée de néo-utilisateurs sur Twitter, et nous observons ce phénomène des Printemps arabes, en passant par les attentats en France de 2015 et 2016, ou encore dans les grands moments électoraux.

![Capture : tweet 1](/images/content/1585335475.png)
![Capture : tweet 2](/images/content/1585335541.png)
![Capture : tweet 3](/images/content/1585335572.png)

Toutefois, et nous masquons à dessein les éléments identitaires de certains des messages relevés, on peut s’étonner de voir dans cette échantillon de néo-utilisateurs des publications émises par des comptes qui ont une épaisseur existentielle congrue et un discours éminemment formaté, et qui peuvent, légitimement, donner à penser que nous avons davantage affaire à des éléments de langage disséminés via des _proxies_ sur les réseaux sociaux, qu’à de réelles convictions. 

De là à dire que la Chine utilise tous les leviers possibles de l’influence digitale ?
