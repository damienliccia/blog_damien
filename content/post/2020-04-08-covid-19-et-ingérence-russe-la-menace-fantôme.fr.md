---
title: 'COVID-19, et ingérence russe, la menace fantôme ? '
author: Damien Liccia
date: '2020-04-08 00:18:00+02:00'
lastmod: '2020-04-08 18:00:00+02:00'
slug: covid-19-et-ingérence-russe-la-menace-fantôme
categories:
  - Fake News
  - Opinion
  - Stratégies d'influence
tags:
  - COVID-19
  - Projet de recherche
draft: no
type: post 
image: reframing_russia.jpg
description: "Analyse d‘un article de Reframing Russia sur le suivi des opérations de guerre informationnelle attribuées à la Russie."

---

_Cette brève est issue du [bulletin](https://www.antidox.fr/2020/04/08/veille-strategique-et-dopinion-digitale-covid-19-8-avril/) réalisé quotidiennement par [Antidox](https://www.antidox.fr/) et ESL._

Kevin Limonier, maître de conférences en études slaves à l’Institut Français de Géopolitique (IFG) et spécialiste de l’écosystème informationnel russe, s’est fait l’écho hier soir d’un [article universitaire](https://reframingrussia.com/2020/04/06/covid-19-disinformation-two-short-reports-on-the-russian-dimension/) publié par deux chercheurs de Manchester appartenant à [_Reframing Russia_](https://reframingrussia.com/), un projet de recherche dédié à l’analyse de l’environnement médiatique post-guerre froide mettant particulièrement la focale sur l’activité de _RT_ (ex-Russia Today). 

<center>

{{< tweet 1247656866269794304 >}}

</center>

Les deux chercheurs qui en sont à l’origine, Stephen Hutchings et Vera Tolz, ont analysé en détail certains écueils inhérents au fact checking et, plus globalement, aux activités de veille et de détection de circulation d’informations de la part d’organisations proches de l’Union européenne. Paradoxalement, les deux chercheurs soulignent qu’en raison de biais, méthodologiques, culturels ou encore géopolitiques, une partie des activités de contre-désinformation pourrait s’avérer au mieux inopérantes, au pire constituer des vecteurs de diffusion de désinformation. Les deux chercheurs soulignent notamment que dans le cadre de leur suivi des informations relatives au COVID-19, les équipes collaborant pour la task force EUvsDisinfo ont commis des biais méthodologiques qui ne sont pas sans conséquences sur les narratifs que tendent à reprendre tout une frange des médias internationaux. Dans certains cas, des phrases et des déclarations sont ainsi extraites de leur contexte, remaniées et décontextualisées, pour constituer des exemples prétendument édifiants des menées de médias, et autres affidés, pro-Kremlin dans l’écosystème informationnel (radio, télévision, média ou encore réseaux sociaux). 

Par exemple, dans le cadre de débats ayant lieu sur des chaînes russes, où de manière polyphonique différentes grilles de lecture s’entrechoquent autour du COVID-19, de ses origines aux ressorts de sa diffusion, les analystes de EUvsDisinfo seraient ainsi enclins à mettre l’emphase spécifiquement sur les déclarations les plus dissonantes pour donner corps à leur grille de lecture originelle. Stephen Hutchings et Vera Tolz questionnent également la manière dont la structure organisationnelle, tant le profil des analystes que ceux dont les activités sont outsourcées, de ces entités de contre-désinformation peut créer des situations équivoques. D’après une étude citée par les deux chercheurs, un bénévole collaborant pour EUvsDisinfo serait ainsi à l’origine de 25% des cas de désinformation mis au jour par cette entité. 

Nul besoin de souligner l’inévitable biais humain introduit tout à la fois par ce déséquilibre et cette part de voix considérable prise par un seul et unique opérateur. De même, le fait q’une large frange de ces bénévoles proviennent de pays ayant connu une présence soviétique, n’est pas sans donner lieu à des schémas de réactivation de grilles de lecture biaisées à l’égard de la Russie. Un appel à la prudence donc, alors même que les ingérences et activités informationnelles tendraient à redoubler en cette période de crise sanitaire.
