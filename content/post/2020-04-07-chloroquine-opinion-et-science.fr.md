---
title: 'Chloroquine, opinion et science'
author: Damien Liccia
date: '2020-04-07 19:32:45+02:00'
lastmod: '2020-04-07 19:41:00+02:00'
slug: chloroquine-opinion-science
categories:
  - Science
  - Opinion
tags:
  - Science
draft: no
type: post
image: discours_methode_descartes.jpg
description: "Série de brèves sur la chloroquine, et les rapports entre opinion et faits scientifiques, issues du bulletin réalisé  quotidiennement par Antidox et ESL."
---

_Série de brèves sur la chloroquine, et les rapports entre opinion et faits scientifiques, issues du bulletin réalisé quotidiennement par [Antidox](https://www.antidox.fr/) et ESL._

## __24/03 - Didier Raoult, nouvelle icône des “populistes” ?__

![Panoramix](/images/content/panoramix.png)

En l’espace de quelques jours, l’infectiologue marseillais, est devenu une personnalité publique, dépassant largement les frontières de son champ de recherche, pour devenir un phénomène d’opinion. À grand renfort de phrases choc (son [interview dans _La Provence_](https://www.laprovence.com/article/papier/5940125/pr-didier-raoult-je-ne-suis-pas-un-outsider-je-suis-en-avance.html) du 21 mars a donné lieu à __427K engagements sur Facebook__ et __12.5K relais sur Twitter__), tranchant avec les postures habituelles du monde de la recherche, Didier Raoult a polarisé la discussion sur la crise sanitaire. 

De manière schématique, sur Twitter il y a désormais les pro et anti-Raoult. Les tenants d’une __approche universitaire__, recrutés notamment dans les rangs du mouvement _“no fake med/science”_, qui respectent scrupuleusement les protocoles d’évaluation (taille de l’échantillon, randomisation et double aveugle), et de l’autre, un __groupe hétéroclite__, où se côtoient élus (essentiellement de PACA), personnalités médiatiques et militants politiques. Ces derniers se recrutent notamment dans les rangs de l’extrême droite. Cette dernière ayant fait, en l’espace de quelques jours, du professeur marseillais une __parfaite antithèse__, un parfait négatif, de la prétendue incurie du gouvernement et de l’exécutif dans la gestion de la crise sanitaire. 

L'__iconographie relative à Didier Raoult__ est à ce titre révélatrice : évocation de l’imaginaire druidique d’Astérix, du Seigneur des Anneaux, références aux savants fous et géniaux des films grand public ou encore héros de manga.

## __30/03 - "No fake science"" versus Didier Raoult, méthodologie versus pragmatisme ?__

{{< tweet 1244049613113483268 >}}

La controverse autour de la chloroquine ne baisse guère en intensité, bien au contraire. La dernière étude publiée par le Pr. Didier Raoult portant sur 80 patients traités par hydroxychloroquine et azithromycine a relancé de nouvelles polémiques, notamment en raison de prétendues __failles méthodologiques__ dans l’étude de l’équipe de l’infectiologue marseillais. Pour les membres du courant _no fake science_ (chercheurs, docteurs, doctorants, statisticiens ou encore journalistes scientifiques), la controverse telle qu’elle se structure depuis plusieurs jours porte en son sein les germes d’un basculement dans l’appréhension par le grand public des enjeux scientifiques. 

Alors que ce mouvement s’est construit avec pour principal objectif de remettre de la rationalité, de l’objectivité scientifique et de la rigueur méthodologique dans des sujets sensibles et sédimentés au long cours par des grilles de lecture politiques (glyphosate, nucléaire ou encore vaccins), la controverse autour de la chloroquine fait vaciller leur entreprise. De quoi donner à l’engouement né autour des travaux du Professeur Raoult les traits d’un __“populisme scientifique”__ selon eux. 

Les partisans du __tout-méthodologique__, sans laquelle la science n’est au demeurant plus grand chose, sont sous le feu nourri des tenants d’un certain __pragmatisme__, justifié par la gravité des circonstances présentes. 

Pour les tenants de cette dernière approche, la rigueur méthodologique s’assimile quant à elle de plus en plus à une forme de lobby.

## __06/04 - Gilets jaunes et chloroquine, même combat ?__

![Étude de l'Ifop](/images/content/etude_ifop_gilets_jaunes_chloroquine.png)

L’Ifop a analysé les assises socio-démographiques du phénomène d’opinion auquel la chloroquine a donné lieu. [Les résultats de cette étude](https://www.ifop.com/wp-content/uploads/2020/04/117231_rapport_Ifop_Labtoo_Cloroquine_2020.04.05.pdf) tendent à confirmer et valider les premiers enseignements issus d’une analyse de la conversation autour de cette controverse en ligne, tant sur Twitter que sur nombre de groupes Facebook. Même si les partisans de la chloroquine tendent à se recruter davantage parmi les 50 à 64 ans (64%), __l’âge n’apparaît comme un facteur déterminant pour expliquer le positionnement des Français sur le sujet__. Un constat similaire se dégage de l’analyse de la taille de l’agglomération de résidence des participants. 

A contrario, et sans toutefois que les écarts ne soient conséquents, le __niveau de diplôme__ apparaît comme étant davantage discriminant. Les Français ayant un diplôme inférieur au BAC apparaissent ainsi comme davantage enclins à appréhender le traitement proposé par l’IHU de Marseille comme potentiellement efficace. Par ailleurs, la grille de lecture à l’égard du mouvement des Gilets jaunes semble être opérante pour catégoriser les attitudes des Français à l’égard de la chloroquine. En effet, 80% des Français se sentant proches des Gilets jaunes considèrent la chloroquine comme efficace. 

Sur l’échiquier politique, __La France Insoumise (LFI)__ semble fournir un contingent significatif de pro-chloroquine (80% des sympathisants du parti de Jean-Luc Mélenchon considèrent la chloroquine comme efficace), devant __Les Républicains (LR)__ dont 71% des sympathisants partagent cette opinion, ceux-ci étant peut-être influencés par les déclarations de Valérie Boyer ou encore de Christian Estrosi, touchés par le COVID-19 et soignés par les équipes du Professeur Didier Raoult. Le __Rassemblement National (RN)__ arrive en 3e position, avec 66% de sympathisants estimant la chloroquine efficace. A contrario, les sympathisants de LREM sont les moins convaincus par l’efficacité de la chloroquine (45%). 

Par ailleurs, l’étude fait apparaître __un léger substrat de complotisme__ de la part d’une frange des Français convaincus de l’efficacité de la chloroquine, ceux-ci partageant pour une partie d’entre eux l’opinion de l’origine non naturelle de la pandémie. Ces derniers étant __65% à considérer qu’elle a été fabriquée dans un laboratoire__. 

La chloroquine, loin d’avoir ébranlé le spectre politique, a davantage agi comme un réactif révélant à nouveaux frais les clivages qui structurent le champ politique, ainsi que leur dimension ubiquitaire. Ces derniers se nourrissent de toutes les controverses (sociales, économiques, religieuses ou encore politiques), et même celles relevant du monde scientifique. De là à postuler que l’autorité scientifique va être à nouveau mise à mal par ce phénomène d’opinion ?


## __07/04 - Quand le _New York Times_ se prend les pieds dans les ficelles rhétoriques de la pensée alternative__

![Une du New York Times](/images/content/trump_nyt.png)

Dans un [article](https://www.nytimes.com/2020/04/06/us/politics/coronavirus-trump-malaria-drug.html) publié hier, le _New York Times_ est revenu sur l’obsession et le militantisme de Donald Trump en faveur de l’utilisation de l’hydroxychloroquine. Un traitement qui divise la communauté scientifique et au sujet duquel Anthony Fauci, éminent immunologiste américain, directeur de l’institut national des maladies infectieuses et chargé de conseiller Donald Trump sur l’épidémie, ne cache pas ses réserves. 

Le quotidien souligne notamment que Donald Trump a, à plusieurs reprises, promu de manière “agressive” et somme toute théâtrale l’utilisation de l’anti-paludique. Une promotion qui s’est notamment faite au nom d’une __supposée supériorité du bon sens__, dont on peut douter que la définition de Trump s’inscrive dans la lignée du Descartes des méditations métaphysiques. 

À maints égards, et cela ne manque pas d’étonner et de questionner sur les mouvements de plaques tectoniques induits par la crise sanitaire présente, les déclarations de Donald Trump ne vont pas sans résonner avec la manière dont tout une partie de la controverse autour de la chloroquine s’est nouée en France. Pourtant, et cela est somme toute assez rare pour être souligné, pour une fois Donald Trump apparaît, de manière momentanée et épisodique certes, comme une victime dans cette affaire. 

Non pas une victime des _“fake news média”_, comme ce dernier se plaît à qualifier le Quatrième pouvoir, mais d’une publication pour le moins spécieuse du New York Times. En quelques paragraphes, les journalistes ayant commis cet article se sont fait les dignes épigones des hérauts de certains travers du net, en proposant une analyse causale de l’_advocacy_ trumpienne. Donald Trump ayant des actions chez Sanofi, il serait d’autant plus enclin à promouvoir l’hydroxychloroquine, que l’on retrouve dans le Plaquenil, produit par Sanofi. CQFD et Deus ex machina. 

Or, et si l’on en croit [Frédéric Bianchi](https://twitter.com/FredericBianchi/status/1247440110204915713), journaliste économique à BFM TV, en 2016 Trump détenait moins de 100K$ d’un fonds, lui-même actionnaire de Sanofi. Un somme bien dérisoire, dont on peut douter qu’elle permette au déjà milliardaire d’accroître sa fortune en cas de ruée vers le Plaquenil. À bien des égards, cette information, et nous nous garderons bien de faire un usage excessif du vocable galvaudé de fake news, n’est pas sans entrer en résonance avec certaines fausses nouvelles, rumeurs et autres bobards qui circulent depuis maintenant plusieurs semaines sur les réseaux sociaux. Et l’on pense ici aux rumeurs entourant le laboratoire P4, d’où aurait prétendument été créé ex nihilo le virus, et auquel Agnès Buzyn et son mari seraient associés. De même que le COVID-19, comme la peste en son temps, n’épargne ni les riches, ni les pauvres, et ne semble guère faire de cas des positions sociales, le virus des fausses nouvelles, indistinctement se retrouve dans d’obscures plateformes et dans les colonnes de prestigieux quotidiens internationaux.
