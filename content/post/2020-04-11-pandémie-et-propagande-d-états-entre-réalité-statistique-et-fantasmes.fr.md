---
title: 'Pandémie et propagande d’États, entre réalité statistique et fantasmes'
author: Damien Liccia
date: '2020-04-11 11:35:00+02:00'
lastmod: '2020-04-11 11:37:00+02:00'
slug: pandémie-et-propagande-d-états-entre-réalité-statistique-et-fantasmes
categories:
  - Fake News
  - Stratégies d'influence
tags:
  - propagande
  - influence
  - manipulation
  - désinformation
  - média
draft: no
type: post 
image: xinhuanet.png
description: "Analyse d'une étude de The Computational Propaganda Project sur les stratégies d'influence des pays et régimes autoritaires pendant la pandémie de COVID-19"
---

_Cette brève est issue du [bulletin](https://www.antidox.fr/2020/04/10/veille-strategique-et-dopinion-digitale-covid-19-10-avril/) réalisé quotidiennement par [Antidox](https://www.antidox.fr/) et [ESL](https://www.eslnetwork.com/)._

The Computational Propaganda Project (COMPROP) de l’OII (Oxford Internet Institute), dont les travaux portent sur les actions de propagande étatique dans le cyberespace, a publié le 8 avril une [pré-étude](https://comprop.oii.ox.ac.uk/wp-content/uploads/sites/93/2020/04/Coronavirus-Coverage-by-State-Backed-English-Language-News-Sources.pdf) relative à l’activité de la Chine, de l’Iran, de la Russie et de la Turquie dans le cadre du contexte sanitaire actuel. Pour mesurer les actions entreprises par ces différents pays, qui font figure depuis plusieurs années maintenant d’usual suspects, les chercheurs de COMPROP se sont intéressés à l’activité des médias anglophones proches ou émanant directement de ces pays. Leurs conclusions mettent au jour une activité moindre de ces “state-backed English-language news outlets” par rapport aux autres médias, mais des engagements suscités (au sens de relais sur les réseaux sociaux) dix fois plus importants que ces derniers. 

En analyse de l’information, ce type d’approches comparatives, toutes choses égales par ailleurs, s’assimile aux travaux d'échantillonnage et d’observations de phénomènes qui ont cours dans les sciences dites “dures”. Cela permet de restituer dans un contexte informationnel plus large des activités données et, par-là même, d’avoir une vision de la portée réelle des actions entreprises. 

Les travaux monographiques mis à part, ces approches analytiques constituent donc le gold standard de la discipline. Un sorte de seuil méthodologique au-delà duquel, sur des sujets aussi sensibles que ceux relatifs à la “guerre informationnelle”, des garde-fous permettent de garantir un maximum d’objectivité et de science-based social science. 

Concernant l’analyse des narratifs de ces médias, les chercheurs soulignent également qu’ils seraient enclins à mettre l’emphase sur les limites des régimes démocratiques dans la gestion du COVID-19 et, par une logique antithétique, à brosser un portrait favorable aux régimes autoritaires. 

Via ces canaux d’information, ou devrions-nous dire de dissémination d’informations cadrées, ces régimes mettent en avant leurs succès, leur leadership, leurs avancées en matière de science et de technologie, ainsi que leur solidarité internationale. Autre élément, l’étude de COMPROP souligne que ces médias soutenus par des régimes autoritaires seraient enclins à faire circuler des “théories du complot” sur la pandémie. Par ailleurs, et bien que cela puisse donner l’impression d’être un truisme, les activités à l’international de ces plateformes tranchent avec les restrictions relatives aux réseaux sociaux, et plus généralement à internet, qui s’appliquent dans chacun de ces pays. 

Ainsi de la Chine où Facebook et Twitter sont tout bonnement interdits, de l’Iran où les restrictions d’accès à ces deux réseaux sociaux se sont multipliées au cours de ces dernières années, ou encore de la Russie qui, bien qu’elle permette l’accès à ces espaces, n’en pratique pas moins des actions coercitives à l’égard de la liberté d’expression. Autant d’éléments qui plaident en faveur d’une compréhension rigoureuse de ces actions sémantiques pour comprendre quelles sont les cibles, les relais et l’efficacité de ces entreprises qui demandent des moyens conséquents.