---
title: Généalogie, enjeux et perspectives de la mise à l'agenda des problématiques
  relatives aux fausses informations par le législateur en France
author: Damien Liccia
date: '2020-03-25 00:00:00+02:00'
lastmod: '2020-03-25 00:00:00+02:00'
slug: projet-de-recherche
categories:
  - Fake News
  - Opinion
tags:
  - Projet de recherche
  - Doctorat
draft: no
type: post
description: "Description de mon projet de thèse"
---

## Résumé

Ce projet de thèse se propose d’analyser l'appréhension par les acteurs politiques français des enjeux liés aux fausses informations. Notre approche de cette thématique se veut holistique et englobe tout à la fois la mise à l'agenda des discours dits conspirationnistes, des discours de haine, de la propagande émanant de l'étranger, des *fake news* et des *deepfakes*. Quels sont les ressorts objectifs et quantifiables à l'origine de cette volonté de réguler les réseaux sociaux ? Quelles sont les conséquences directes et induites de cette construction des réseaux sociaux comme un espace prétendument anomique ? Quels sont les écueils inhérents au recours accru aux algorithmes et au *digital labor* par les GAFAM pour répondre aux attendus du gouvernement français ? Si le sujet *fake news* irradie depuis 2016, sous l'effet conjoint du Brexit et de l'élection de Donald Trump aux États-Unis, la littérature sur ce sujet, ce dernier n'en demeure pas moins caractérisé par une ductilité problématique, une conceptualisation erratique, où s'entremêlent parfois une dimension recherche et des stratégies militantes. L'existence de deux grilles de lecture à l'égard des *fake news*, l'une qui met l'accent sur la crise générale de la vérité et le prétendu basculement dans l'ère de la *post truth*, et l'autre qui insiste davantage sur les menées de puissances étrangères pour saper la confiance générale dans les démocraties, contribuent à accroître la difficulté à définir le phénomène. Les deux ne sont pas antinomiques et bien souvent l'une et l'autre sont utilisées, alternativement ou conjointement, pour décrire des phénomènes d'opinion. Par ailleurs, cette focalisation autour des effets prétendument sans bornes et sans limites de cette hypothétique mutation de notre rapport au réel et à la vérité sur les réseaux sociaux soulève également une série de questions quant à l'évaluation desdits effets de ce phénomène. 

## Contexte

De la mise à l’agenda politique, lors du quinquennat de François Hollande, des enjeux informationnels et sécuritaires liés à une visibilité prétendument accrue des discours dits conspirationnistes (M. Foulot, 2015 ; R. Josset, 2015, R. Badouard, 2018), à celle, plus récente, des craintes suscitées par l’essor des fake news sur les réseaux sociaux (F.-B Huyghe, 2018), une partie significative des acteurs politiques, associatifs, militants, universitaires et médiatiques a contribué à façonner un rapport problématique et conflictuel avec les discours se structurant sur les réseaux sociaux et les médias numériques (G.Bronner, 2015 ; C.Salmon 2019). Or, alors même que la réaction du législateur s’est caractérisée par une approche volontariste (Loi « fake news » et loi Avia en 2019), le flou propre à ce phénomène n’a jamais été réellement dissipé, tant dans son essence que dans ses caractéristiques et dans sa supposée influence. 

De l’origine du concept lui-même et des différents vocables connexes qui ont vu le jour dans son sillage, à ses manifestations concrètes sur les réseaux sociaux, le concept de fake news concentre, à lui-seul, une partie des ambiguïtés et des peurs entretenues par les acteurs politiques à l’égard des réseaux sociaux. D’espaces ouverts, vecteurs de toutes les utopies, ces différents dispositifs numériques sont désormais essentiellement appréhendés sur un mode foncièrement dystopique. À l’éloge dual de l’ouverture, avec l’accroissement des échanges entre individus et, corrélativement, des externalités positives politiques consécutives de cette communication accrue, s’est ainsi substitué un tropisme régulateur, voire coercitif, dont la France constitue l’une des figures de proue.

## Objectifs

Notre problématique, qui fait du périmètre français son terrain d’investigation privilégié, tout en n’excluant de mobiliser des exemples à l’international, se veut duale. Elle cherche tout à la fois à appréhender l’effectivité de ces phénomènes informationnels, leurs caractéristiques et leur efficacité en termes de cadrage de l’opinion (E. Bernays, 1928 ; V. Klemperer, 1947 ; P. Bourdieu, 1985 ; G. Lakoff, 2015), ainsi qu’à comprendre de quelle manière les décideurs élaborent leur grille de lecture, leur discours et leur action à partir de cet état de fait.

En retraçant notamment le développement du terme fake news dans de débat public, via l’exploitation des données issues de l’open data (procès verbaux des séances de l’Assemblée nationale, du Sénat, discours des membres du gouvernement et du Président de la République), des traces numériques laissées par les parlementaires sur les réseaux sociaux ainsi que les discours tenus par des groupes spécifiques (journalistes, acteurs associatifs, universitaires…), il s’agira de déterminer les différentes phases de la construction du problème fake news dans un pays qui, a contrario de ce qui a pu se dérouler dans le cadre de la conversation numérique relative au Brexit ou de l’élection présidentielle de 2016 aux États-Unis, n’a vraisemblablement jamais été réellement confronté à des attaques massives émanant d’acteurs supposément coutumiers de ce type de pratiques, à l’instar de la Russie par exemple. 

Notre approche cherche à déterminer de quelle manière le terme de fake news a pu s’imposer dans le discours politique et médiatique, au point de phagocyter et d’englober des concepts pourtant bien plus étayés, notamment dans le domaine universitaire, comme ceux de propagande (E.Bernays, 1928 ; S.Tchakhotine, 1939  ; J.Ellul, 1962 ) ou de désinformation (F.-B Huyghe, 2015). Est-ce à dire que la nouveauté supposée desdites fake news résiderait en elle-même, dans leur circulation, leurs caractéristiques et leur provenance, ou bien, au contraire, davantage dans des choix narratifs et stratégiques impulsés par des acteurs politiques ? 

Avec cette prétendue mutation de la frontière entre le vrai et le faux, et son corolaire constitué par l’influence supposément accrue du faux, se dessine en filigrane une réflexion globale sur la perception des réseaux sociaux en tant que média et leurs singularités, ou continuité, par rapport à leurs devanciers, de la presse dite traditionnelle, en passant par la radio ou plus récemment la télévision. En d’autres termes, si ce phénomène propre à la couche sémantique du cyberespace a donné lieu à la construction d’un problème d’ordre politique et à l’élaboration de dispositifs réglementaires, est-ce à supposer que son influence est supérieure à la télévision ou la radio ? 

Cette problématique de l’influence des réseaux sociaux numériques achoppe pourtant sur un paradoxe qui, en l’état, n’a été résolu par aucune étude scientifique. Si la quantification de ces discours est possible que ce soit en termes d’engagements suscités (retweets, likes, shares ou encore views), de nombres de publications émises (nombre de tweets, de posts Facebook ou de vidéos sur YouTube) ou encore de conversations engendrées via des méthodologies d’analyse de données, la percolation entre le virtuel et le réel, autrement dit le prétendu continuum entre une fake news à dimension politique et électorale et un changement dans le vote d’un groupe donné d’individus n’a pas pu être êtredémontrée pour l’heure. Partant d’un raisonnement analogique, assez proche de celui qui anime une partie des acteurs ayant préempté cette problématique, nous pouvons considérer que les fake news, de la même manière que les controverses relatives à la santé publique, posent fondamentalement un enjeu en termes de santé informationnelle. Or, contrairement à ces dernières, l’impossibilité d’en mesurer les effets concrets et réels aurait dû constituer un écueil qui, à lui seul, doit contribuer à nuancer une partie significative des discours qui se sont construits autour de ce phénomène.

De même, l’effectivité et la capacité de persuasion ou de deception, pour reprendre un terme militaire anglo-saxon, de ces menées informationnelles sur les réseaux sociaux numériques résonnent logiquement avec les travaux menés par Paul Lazarsfeld, notamment dans *The people’s choice* paru en 1944, qui avaient grandement, et durablement, contribué à démystifier le postulat d’une toute-puissance des médias. Les réseaux sociaux invalideraient-ils cette théorie ? Leur capacité de pénétration serait-elle supérieure à tous les autres médias ? Alors que d’aucuns pointent du doigt une écologie de l’attention fragmentée et éclatée sur les réseaux sociaux, en raison de la circulation en flux de l’information, comment postuler une influence supérieure des réseaux sociaux numériques sur des médias dits de masse ?

Bien que global, et relevant tout à la fois des relations internationales, du droit international, de la géopolitique ou encore de la stratégie, l’appréhension des activités informationnelles attribuées à la Russie (D. Patrikarakos, 2017; K.Limonier, 2018) dans l’espace informationnel français constitue, à maints égards, un point d’entrée heuristique probant pour aborder la dialectique se nouant entre de prétendues actions dans le cyberespace et leur appréhension par les décideurs. 

Le cas des ingérences informationnelles attribuées à la Russie dans le cadre de processus électoraux ou controverses d’opinion domestiques (de l’élection de mai 2017, à l’affaire Benalla en passant par le mouvement des « Gilets jaunes ») propose un exemple saillant qui permet de venir questionner l’élaboration d’imaginaires et de narratifs structurants, ainsi que d’ouvrir une réflexion sur la construction de la figure d’ennemi (C. Schmitt, 1932 ;  U. Eco, 2014) à l’ère du numérique avec les fake news comme pierre angulaire de ces différents processus. L’identification de la Russie comme un pourvoyeur et un archétype de la menace informationnelle constitue ainsi un exemple intéressant permettant de mettre au jour les menaces perçues, qu’elles soient réelles ou non, par un système politique. Il s’agit donc de retracer et de problématiser, via une approche diachronique, la construction au niveau français de cet objet spécifique ; en partant toutefois du postulat que sur ce sujet le cas français ne saurait être pleinement disjoint des autres démocraties occidentales, tant au niveau européen avec l’Union européenne, militaire avec l’OTAN ou international (G7, G20, ONU). 

Notre approche s’intéresse donc moins aux fake news en elles-mêmes qu’aux discours qu’elles suscitent. Il s’agit donc non pas tant d’investiguer les ressorts propres aux acteurs utilisant ce registre informationnel pour poursuivre des finalités données que d’appréhender la capacité desdites actions à produire des effets politiques. Ce que nous pouvons qualifier de fièvre obsidionale informationnelle, qui a connu un pic significatif au cours des années 2018 et 2019, constitue donc l’axe majeur de notre recherche, afin d’en déterminer le processus sous-jacent.

Cette focale mise sur l’influence supposée des fausses informations dans le cadre des controverses, des débats publics et des processus électoraux soulève des questionnements multiples : 

*	Ce discours est-il la résultante logique d’un phénomène nouveau, aux contours clairs et pouvant être quantifiés ? 
*	Si l’on postule l’effectivité dudit phénomène quelles sont les hypothèses étiologiques qui doivent être investiguées pour en rendre raison ?  
*	Que dit la construction du problème lié aux fausses informations sur l’état de l’opinion ou, a contrario, sur les grilles de lectures qui animent les décideurs qui préemptent ce terrain ? 
*	Quels sont les conséquences envisageables des dispositifs réglementaires mis en place ces dernières années pour lutter contre les théories du complot, les fake news ou les discours de haine ? 
*	Que dit la prégnance de l’intelligence artificielle (D.Cardon, 2015) dans le discours conjoint des acteurs politiques et des acteurs gérant lesdits espaces informationnels sur les dispositifs élaborés pour répondre au problème ?


## Méthodologie

Cette recherche s’inscrit dans le champ des *digital humanities* et est guidée par un souci constant d’hybridation entre outils numériques, programmation et sciences humaines et sociales. Notre approche est notamment articulée autour de l’exploitation de corpus issus de données Twitter, que ce soit en investiguant les données rendues publiques par le réseau social dans le cadre de son programme « Elections integrity », ou en récupérant sur des périmètres définis des données via du *scrapping* afin de récupérer des données historiques.

Pour mener à bien nos travaux nous nous appuierons tout particulièrement sur :

-	Le corpus rendu public en octobre 2018 relatif à l’activité de 3 613 comptes identifiés par Twitter comme appartenant à l’Internet Research Agency (IRA) ;
o	Le corpus complémentaire de 416 comptes appartenant à l’IRA publié par Twitter en Janvier 2019 ;
-	Un corpus de 4 022 000 tweets relatifs à la discussion autour du référendum catalan entre le 1er août et le 1er décembre 2017 ;
-	Un corpus de 566 302 tweets relatifs à l’affaire Benalla récupérés entre le 17 et le 27 juillet 2018 ; 
-	Un corpus de 1 504 316 tweets en langue française émis entre le 10 novembre et le 4 décembre dans le cadre de la discussion relative du mouvement des Gilets jaunes, ainsi qu’un corpus anglophone de 95 835 tweets émis sur la même période. Sur Facebook, un corpus de 1 533 240 commentaires réalisés à partir de 11 pages de médias français ;
-	Les articles publiés par Sputnik France, de la création du média jusqu’à mars 2019, ainsi que l’ensemble des tweets relayant ses articles, ainsi que la timeline complète du compte français de ce média ;
-	Des corpus de médias français (Le Monde, Le Figaro, Libération…) sur une série de mots-clés afférents à la thématique afin de déterminer l’évolution de leur appréhension du sujet ;
-	Des corpus issus des publications scientifiques, des travaux de recherche et des communications saillantes relatives aux fake news et à la désinformation. L'analyse portera notamment sur les publications de *think tanks* anglo-saxons (Rand, Atlantic Council...), les publications d'organismes spécialisés (IRSEM, NATO StratCom COE...) ou encore d'associations et d'ONG (Avaaz, Disinfo Lab...) ;
-	Les données parlementaires, gouvernementales et présidentielles issues de l’open data ;
-	Des données issues des statistiques de consultation de pages Wikipédia données ;
-	Des données issues de pages Facebook, comptes Instagram ou profils YouTube ;
-	Une exploitation, via des processus de scraping, des données référencées par les moteurs de recherche autour de thématiques données ;
-	Les tendances proposées par Google Trends.

L’exploitation des données est réalisée par le langage *R* qui permet tout à la fois de récupérer, nettoyer, structurer, modéliser et visualiser des données. Afin d’exploiter nos différents corpus nous recourrons au *data* et *text mining*, en nous inspirant notamment des travaux entrepris notamment aux États-Unis par la chercheuse et data scientist Julia Silge, au *machine learning* (Scott V.Burger, 2018 ; J.M Poggi, R.Genuer, 2019) et à la *data visualisation* (H.Wickham, 2005). Cette approche hybride doit beaucoup également aux travaux menés par le chercheur français Joël Gombin, qui a contribué à structurer cette discipline à la lisière de l’analyse de données et des sciences humaines

\newpage


## Bibliographie

* __Arendt, H__. (1972). Du mensonge à la violence: essais de politique contemporaine. Paris, France: Calmann-Lévy.

* __Badouard, R__. (2017). Le désenchantement de l'internet. Limoges, France: FYP editions.

* __Bernays, E. L__. (1928). Propaganda: comment manipuler l'opinion en démocratie. Paris, France: Zones.

* __Bloch, M__. (1921). Réflexions d’un historien sur les fausses nouvelles de la guerre. Paris, France: Revue de synthèse historique.

* __Bourdieu, P__. (1985). Ce que parler veut dire : L'économie des échanges linguistiques. Paris, France: Fayard.

* __Burger, S. V__. (2018). Le machine learning avec R: Pour une modélisation mathématique rigoureuse. Sebastopol, États-Unis: O'Reilly.

* __Bronner, G__. (2013). La démocratie des crédules. Paris, France: Presses Universitaires de France.

* __Cardon, D__. (2010). La démocratie Internet: promesses et limite. Paris, France: La République des idées.

* __Cardon, D__. (2015). A quoi rêvent les algorithmes: nos vies à l'heure des big data. Paris, France: La République des idées.

* __Cardon, D__. (2019). Culture numérique. Paris, France: Les Presses de Sciences Po.

* __Casili, A__ & __Cardon, D__. (2016). Qu’est-ce que le digital labor ?: Les enjeux de la production de valeur sur Internet et la qualification des usages numériques ordinaires comme travail. Paris, France: INA.

* __Casili, A__. (2019). En attendant les robots. Paris, France: Le Seuil

* __Debray, R__. (2000). Introduction à la médiologie. Paris, France: Presses universitaires de France.

* __Detienne, M__. (2006). Les maîtres de vérité dans la Grèce archaïque. Paris, France: LGF/Le Livre de Poche.

* __Eco, U__. (1985). La guerre du faux. Paris, France: Le Livre de Poche.

* __Eco, U__. (2016). Construire l'ennemi et autres écrits occasionnels. Paris, France: LGF/Le Livre de Poche.

* __Ellul, J__. (1962). Propagandes. Paris, France: Economica.

* __Girardet, R__. (1986). Mythes et mythologies politiques. Paris, France: Seuil.

* __Habermas, J__. (1987). Théorie de l'agir communicationnel. Paris, France: Fayard

* __Huyghe, F.-B__. (2008). Maîtres du faire croire: de la propagande à l'influence. Paris, France: Vuibert.

* __Huyghe, F.-B__. (2001). L'ennemi à l'ère numérique: Chaos, information, domination. Paris, France: Presses Universitaires de France - PUF.

* __Huyghe, F.-B__., __Kempf, O__., & __Mazzucchi, N__. (2015). Gagner les cyberconflits: Au-delà du technique. Paris, France: Economica.

* __Huyghe, F.-B__. (2016). La désinformation: les armes du faux. Paris, France: Armand Colin.

* __Huyghe, F.-B__. (2018). Fake news: la grande peur. Versailles, France: VA Press éditions.

* __Josset, R__. (2015). Complosphère: l'esprit conspirationniste à l'ère des réseaux. Paris, France: Lemieux éditeur.

* __Kershaw, I__. (1995).  L'opinion allemande sous le nazisme. Paris, France : CNRS.

* __Klemperer, V__. (1998). Lti, la langue du IIIème Reich. Paris, France: Pocket.

* __Lakoff, G__. (2015). La guerre des mots : ou comment contrer le discours des conservateurs. Paris, France: Zones.

* __Laborie, P__. (2001). L'opinion française sous Vichy: les Français et la crise d'identité nationale, 1936-1944. Paris, France: Seuil.

* __Lazarsfeld, P__. (1952) People’s Choice : How the voter makes up his mind in a presidential campaign. États-Unis: Columbia University Press.

* __Le Bon, G__. (1895). Psychologie des foules (9ème éd.). Paris, France: PUF.

* __Limonier, K__. (2018). Ru.net: géopolitique du cyberespace russophone. Paris, France: L'Inventaire.

* __Patrikarakos, D__. (2017). War in 140 Characters: How Social Media Is Reshaping Conflict in the Twenty-First Century. New-York, États-Unis: Basic Books.

* __Paveau, M. A__. (2017). L'analyse du discours numérique: dictionnaire des formes et des pratiques. Paris, France: Hermann.

* __Reichstadt, R__. (2019). L'opium des imbéciles: Essai sur la question complotiste. Paris, France: Grasset

* __Salmon, C__. (2019). L'ère du clash. Paris, France: Fayard.

* __Silge, J__., & __Robinson, D__. (2017). Text Mining with R: A Tidy Approach. Sebastropol, États-Unis: O'Reilly.

* __Tchakhotine, S__. (1973). Le viol des foules par la propagande politique. Paris, France: Gallimard.

* __Tréguer, F__. (2019). L'utopie déchue. Une contre-histoire d'Internet XVe-XXIe siècle. Paris, France: Fayard

