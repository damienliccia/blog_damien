---
title: "Lettres capitales et systèmes de classification"
author: "Damien Liccia"
date: '2020-04-03 18:56:12+02:00'
lastmod: '2020-04-03 21:34:48+02:00'
draft: no
slug: lettres-capitales-et-systemes-de-classification
tags:
- data
- machine learning
- R
categories: Data
type: post
description: "Création d’un modèle de classification de tweets basé sur l’utilisation des lettres capitales dans une série de publications."
image: 1585942400.png
---

```{r setup, include=FALSE}
require(knitr)
require(tidyverse)
require(ggrepel)
require(tidymodels)
require(rtweet)
require(kableExtra)
require(lubridate)

knitr::opts_chunk$set(cache = T,
                      warning = F,
                      message = F,
                      echo = T,
                      dpi = 180,
                      fig.width = 8,
                      fig.height = 5)

```

L'analyse de la **ponctuation** et de la **forme des lettres** (majuscule^[On emploie souvent, alternativement et de manière fautive, les termes de majuscule et de capitale. Une majuscule désigne la lettre initiale d'un nom propre ou d'une phrase, là où les capitales sont utilisées à la discrétion de l'auteur pour mettre l'emphase sur une chaîne de caractères spécifique], capitale et minuscule) constitue un axe intéressant pour mettre au jour des caractéristiques et des marqueurs identiaires sémantiques et linguistiques dans un corpus de texte.

Dans une logique de _machine learning_, on peut considérer que l'analyse de la forme des lettres, en complément d'une analyse de la **ponctuation**, de la **tonalité** ou encore des **emojis**, peut permettre de faire ressortir des _patterns_ autres que des unités sémantiques, comme un mot ou groupe de mots (_ngrams_).

Dans certains contextes, on peut également considérer que l'utilisation **synchronique** de lettres capitales est un marqueur d'une forme de violence ou de colère à un moment donné, tandis qu'une utilisation **diachronique** renverrait, soit à une violence dans le temps, soit à une stratégie éditoriale spécifique faisant de l'utilisation accrue des lettres capitales un marqueur identitaire et communicationnel.

En nous intéressant à la communication de deux Chefs d'État, en l'occurrence Donald Trump et Emmanuel Macron, sur le réseau social Twitter on peut appréhender toute la fertilité heuristique des analyses de données basée sur la densité de lettres capitales.


```{r}
tml_macron <- read_csv("https://www.dropbox.com/s/e9b9uzv6qckpyhg/macron.csv?raw=1")
tml_trump <- read_csv("https://www.dropbox.com/s/pwiyxdhl8klixro/trump.csv?raw=1")

join <- rbind(tml_macron,
              tml_trump) %>%
  group_by(username) %>%
  filter(date >= "2018-01-01")

```


Pour ce faire, nous avons récupéré l'ensemble des publications organiques^[On parle de publications ou de tweets organiques par opposition aux retweets] de Donald Trump et d'Emmanuel Macron depuis le 1er janvier 2018.


```{r}

obs <- join %>%
  group_by(username) %>%
  summarise(`Nombre d'observations` = n()) %>%
  rename(`Chef d'État` = 1)
```


Notre corpus de publications est composé des observations suivantes :


```{r, echo = FALSE}
kable(obs) %>%
  kable_styling(bootstrap_options = "striped", full_width = F)

```


À partir de ces données, on peut  calculer la densité de lettres capitales dans chacune des publications de Donald Trump et d'Emmanuel Macron.

Afin d'éviter tous biais, nous devons au préalable supprimer les mentions de comptes, ainsi que les URL :


```{r}

text <- join %>%
  rowwise() %>%
  select(username, tweet, date) %>%
  mutate(text_clean = gsub("@\\S+\\s*", " ", tweet),
         text_clean = gsub("http\\S+\\s*", " ", text_clean),
         text_clean = gsub("https\\S+\\s", " ", text_clean),
         text_clean = gsub("pic.twitter.com/.*", " ", text_clean),
         text_clean = gsub(".*pic.twitter.com", " ", text_clean),
         nchar = nchar(text_clean), 
         nspaces = length(strsplit(text_clean, " ")[[1]]),
         nchar_count = nchar - nspaces,
         text_clean = paste0(tolower(substr(text_clean, 1, 1)), substr(text_clean, 2, nchar)),
         text_clean = gsub("[[:punct:] ]", " ", text_clean),
         cap_count = str_count(text_clean, "[A-Z]"),
         cap_dens = cap_count / nchar_count,
         week = round_date(date, unit = "week")) %>%
  group_by(username, week) %>%
  mutate(cap_dens_weekly = mean(na.omit(cap_dens))) %>%
  ungroup() %>%
  select(-c(nchar, nspaces)) %>%
  filter(nchar_count > 0)

text %>%
  ggplot(aes(x = date, y = cap_dens, color = username)) +
  geom_jitter() +
  theme_minimal() +
  labs(color = "Chefs d'État",
       x = NULL,
       y = "Densité de lettres capitales / tweet") +
  theme(legend.position = "bottom")


```


En représentant graphiquement la densité de lettres capitales par publication de chacun des Chefs d'États dans le temps, on remarque qu'au-delà d'un certain seuil (0,35), et à une seule exception près, seules sont présentes les publications de Donald Trump. 

Quant à celles d'Emmanuel Macron, pour l'essentiel, elles se situent en deçà du seuil des 0,25. 


```{r}
require(lubridate)

text %>%
  mutate(date = floor_date(date, unit = "week")) %>%
  group_by(username, date) %>%
  mutate(moy = mean(cap_dens)) %>%
  distinct(date, .keep_all = T) %>%
  ggplot(aes(x = date, y = moy, color = username)) +
  geom_line() +
  geom_point() +
  theme_minimal() +
  labs(color = "Chefs d'État",
       x = NULL,
       y = "Moyenne hebdomadaire de lettres capitales") +
  theme(legend.position = "bottom")

```


En basculant sur une moyenne hebdomadaire, la dichotomie est encore plus marquée. 

## Construction d'une modèle

Pour construire notre modèle, à partir de notre corpus **text**, nous créons un corpus _train_ et un corpus _test_.


```{r}

text <- text %>%
  mutate(username = as.factor(username))

class(text$username)


text_split <- initial_split(text, strata = username)
text_train <- training(text_split)
text_test <- testing(text_split)

```


La fonction **glm** permet de créer un modèle linéaire généralisé. Notre modèle doit expliquer une variable donnée, en l'occurrence dans notre cas le nom d'un Chef d'État, par une ou plusieurs variables explicatives. Concernant les variables explicatives nous en utilisons deux : 

* _cap_dens_ : la densité de lettres capitales par publication ;
* _cap_dens_weekly_ = la densité de lettres capitales hebdomadaires.


```{r}

summary(model <- glm(username ~ cap_dens + cap_dens_weekly, 
    data = text_train,
    family = "binomial"))

```


En appliquant la fonction **summary** à notre modèle on constate que la variable explicative _cap_dens_weekly_ a une _p-value_ très faible ce qui donne à penser que cette variable joue un rôle important pour déterminer si une publication est davantage le fait de Donald Trump ou d'Emmanuel Macron.


```{r}

text_test <- text_test %>%
  mutate(pred = predict(model, ., type = "response"),
         pred_username = if_else(pred >= 0.5, "Prédiction - Donald Trump", "Prédiction - Emmanuel Macron"))

text_test %>%
  filter(!is.na(pred_username)) %>%
  ggplot(aes(x = date, y = pred, color = pred_username)) +
  geom_jitter() +
  theme_minimal() +
  labs(color = "Chefs d'État",
       x = NULL,
       y = NULL) +
  theme(legend.position = "bottom")

```


Le modèle étant créé, nous l'utilisons sur notre **corpus test**. De manière, quelque peu arbitraire et empirique, nous fixons un seuil à 0,5 pour convertir les probabilités issues du modèle en prédictions catégorielles. Évidemment, on pourrait ajuster la valeur _t_ (_threshold value_) en fonction des erreurs et autres faux positifs engendrés par notre modèle.


```{r, echo=F}


kable(table(text_test$username,
      text_test$pred_username)) %>%
  kable_styling(bootstrap_options = "striped", full_width = F)


```


Pour évaluer la précision de notre modèle de classification basé sur l'analyse des lettres capitales, nous réalisons une **matrice de confusion** sur les données de notre corpus de test.

Il apparaît que ce dernier est relativement performant, avec **14 faux positifs** pour Emmanuel Macron et **24 faux positifs** pour Donald Trump.


