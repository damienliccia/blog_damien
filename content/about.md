---
title: "À propos - Damien Liccia"
type: page
image: logo_otd_twitter.png
description: "Associé fondateur d'IDS Partners, ex-khûbe passé par La Sorbonne, je travaille sur la propagande, les rumeurs, la désinformation et les fake news."
---

Après trois années de classe préparatoire littéraire et un master de communication politique et sociale à Paris 1 Panthéon-Sorbonne, dans le cadre duquel j'ai rédigé un mémoire consacré à la diffusion des discours complotistes sur les réseaux sociaux, j'ai poursuivi mes recherches sur les enjeux liés à la propagande, les rumeurs, la désinformation et les *fake news*. En parallèle de ces travaux, je me suis formé, notamment dans le cadre de mes expériences professionnelles, à l'étude de l'opinon en ligne et à l'analyse de données.

Mes recherches s'appuient entre autres sur le *machine learning* et le *text mining* avec le langage de programmation *R*. Les publications et études que j'ai pu réaliser s'inscrivent dans le domaine des *digital humanities*, à la lisière entre la statistique, la programmation et les sciences humaines et sociales.

# Formation

* Depuis 2019 - Doctorant, Celsa (Sujet de thèse : __"Généalogie, enjeux et perspectives de la mise à l’agenda des enjeux relatifs aux fausses informations par le législateur en France"__)

* 2015-2016 - __Master 2 Communication politique et sociale__, Université Paris 1 Panthéon-Sorbonne, Mention très bien et major de promotion. Mémoire de recherche : Remise en cause des discours officiels, ou l’influence des contre discours sur les médias sociaux - Analyse de la structuration et de la circulation des discours alternatifs lors des attentats de 2015 et de 2016, sous la direction de Justin Poncet (note obtenue 18/20) ;

* 2014-2015 - __Master 1 Science politique__, Université Paris 1 Panthéon-Sorbonne ;

* 2011-2014 - __Classe préparatoire littéraire__, Lycée Victor Duruy (Paris, 7ème), Mentions très bien sur les années de khâgne et de khûbe. Sous-admissible à l’ENS de Lyon (2014)

# Parcours professionnel

* Depuis 2018 - Directeur associé, IDS Partners ;

* Depuis 2019 - Partner, Antidox ;

* 2017-2018 - Analyste de données, Angie ;

* 2016-2017 - Consultant en analyse de l'opinion, Antidox


# Ouvrages et publications


* _Dans la tête des gilets jaunes_, avec __François-Bernard Huyghe__ et __Xavier Desmaison__, Janvier 2019, VA Éditions,

* _Astroturfing et fake activism : la dynamique cachée du boycott marocain_, Septembre 2018, _Jeune Afrique_,

* _Affaire Benalla : une controverse sous influence ?_, Août 2018, _Blog Médium_,

* _Catalogne, la Russie a-t-elle (réellement) essayé de diviser l’Espagne…et l’Union européenne ?_, Avril 2018, _Blog Medium_,

* _La conflictualité informationnelle à l’ère du digital_, Juillet 2017, _Blog Medium_



# Compétences informatiques

* __Système d'exploitation__ : Maîtrise de Linux, OSX et Windows ;

* __Langages de programmation__ : *R* et connaissances en *Python* ;

* __Domaines d'intérêts__ : _text mining_, _machine learning_, _topic modeling_, _data visualisation_ ;

* __Logiciels de cartographie__ : *Gephi* ;

* __Logiciels SIG__ : *QGIS*.
