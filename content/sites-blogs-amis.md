---
title: "Sites et blogs amis"
type: page
---

* Le blog de François-Bernard Huyghe, {{< target-blank "huyghe.fr" "https://www.huyghe.fr/" >}}
* Le blog d'Olivier Kempf, {{< target-blank "EGEA" "http://www.egeablog.net/" >}}
* Le blog de Thibault Lamidel, {{< target-blank "Le Fauteuil de Colbert" "http://lefauteuildecolbert.blogspot.com/" >}}
